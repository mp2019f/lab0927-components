package kr.ac.mju.mp2019f.components;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import static android.content.DialogInterface.*;

public class MainActivity extends AppCompatActivity {

    private RadioGroup rgLunch;
    private TextView textView;
    private SeekBar seekBar;
    private CheckBox checkBox;
    private AlertDialog.Builder adb;
    private AlertDialog ad;
    private Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rgLunch = findViewById(R.id.rgLunch);
        textView = findViewById(R.id.textView);
        seekBar = findViewById(R.id.seekBar);
        checkBox = findViewById(R.id.checkBox);
        button = findViewById(R.id.button);

        rgLunch.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch(i) {
                    case R.id.rbPaul:
                        textView.setText("Today's lunch is Paul Juior");
                        break;
                    case R.id.rbHak:
                        textView.setText("Today's lunch is Hak Kwan");
                        break;
                    case R.id.rbSkip:
                        textView.setText("Today's lunch is Skipped");
                        break;
                }
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                textView.setText(String.valueOf(i)+"/"+String.valueOf(seekBar.getMax()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.d( "CheckBox", String.valueOf(b));
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adb = new AlertDialog.Builder(MainActivity.this);
                adb.setTitle("Question");
                adb.setMessage("Would you like a lunch?");
                adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MainActivity.this.finish();
                    }

                });
                adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }

                });
                ad = adb.create();
                ad.show();
            }
        });


    }
}
